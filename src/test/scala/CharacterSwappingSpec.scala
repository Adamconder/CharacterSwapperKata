import org.scalatest.{MustMatchers, WordSpec}

class CharacterSwappingSpec extends WordSpec with MustMatchers {

  ".swapChar" must {

    "swap x for a y" in {
      CharacterSwapper.swapChar("X") mustBe "Y"
    }

    "swap y for z" in {
      CharacterSwapper.swapChar("Y") mustBe "Z"
    }

    "swap z for x" in {
      CharacterSwapper.swapChar("Z") mustBe "X"
    }

  }

  ".swap" must {

    "swap XY for YZ" in {
      CharacterSwapper.swap("XY") mustBe "YZ"
    }

    "swap XYZ for YZX" in {
      CharacterSwapper.swap("XYZ") mustBe "YZX"
    }

    "swap XXXXX for YYYYY" in {
      CharacterSwapper.swap("XXXXX") mustBe "YYYYY"
    }

  }

  ".compliments" must {

    "generate a set of Y,Z for X" in {
      CharacterSwapper.compliment("X") mustBe Set("Z", "Y")
    }

    "generate a set of X,Z for Y" in {
      CharacterSwapper.compliment("Y") mustBe Set("Z", "X")
    }

    "generate a set of X,Y for Z" in {
      CharacterSwapper.compliment("Z") mustBe Set("Y", "X")
    }

  }

  ".generateCompliments" must {

    "generate YY, ZY, XX, XZ for XY" in {
      CharacterSwapper.generateCompliments("XY") mustBe Set("YY", "ZY", "XX", "XZ")
    }

    "generate YYZ, ZYZ, XXZ, XZZ, XYX, XYY for XYZ" in {
      CharacterSwapper.generateCompliments("XYZ") mustBe Set("YYZ", "ZYZ", "XXZ", "XZZ", "XYY", "XYX")
    }

  }

}
