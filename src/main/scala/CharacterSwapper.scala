object CharacterSwapper {

  def swapChar(s : String) = s match {
    case "X" => "Y"
    case "Y" => "Z"
    case "Z" => "X"
  }

  def swap(chars : String) : String = {
    chars.map(c => swapChar(c.toString)).mkString
  }

  def compliment(chars : String) : Set[String] = {

    def find(c : Char) = c match {
      case 'X' => Set("Y", "Z")
      case 'Y' => Set("X", "Z")
      case 'Z' => Set("Y", "X")
    }

    chars.flatMap(find).toSet
  }

  def generateCompliments(chars : String) : Set[String] = {
    chars.flatMap {
      v =>
        val comps = compliment(v.toString)
        comps.map {
          c =>
            chars.replace(v, c(0))
        }
    }.toSet
  }

}
